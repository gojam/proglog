module gitlab.com/gojam/proglog

go 1.15

require (
	github.com/gogo/protobuf v1.3.1
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.6.1
	github.com/tysontate/gommap v0.0.0-20201017170033-6edfc905bae0
	google.golang.org/genproto v0.0.0-20190819201941-24fa4b261c55
	google.golang.org/grpc v1.26.0
	launchpad.net/gocheck v0.0.0-20140225173054-000000000087 // indirect
)
