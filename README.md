# Distributed Services with Go

Just bought "Distributed Services with Go" from [The Pragmatic Bookshelf](https://pragprog.com/) over Thanksgiving, 2020.  Following along.

## Chapters

* ch1: barebones web server
* ch2: protobuf
* ch3: log

## Notes

### Chapter 2

```
brew install protobuf
go get github.com/gogo/protobuf/...@v1.3.1
```